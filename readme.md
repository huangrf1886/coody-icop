
## Coody Framework

#### 前言：

    Coody Framework 是由笔者业余时间编写的一套Ioc框架。由最初的项目内置代码发展至今的Maven中央仓库。历时4个月有余。

    而在今后的时间里，Coody Framework将不断完善，争取今年内发布一套成熟的release版本。同时，笔者也会自带成功案例。

#### 用途与优势：
    
    Coody Framework是一套轻量化Ioc框架。除分布式(即Rcc模块)未完成外，其余均已经过笔者本地测试。全框架大小214kb。依赖cglib-3.2、log4j-1.2、noson-1.0.7。

    基于Coody Framework框架的web项目启动耗时约370ms(在i5 2320上所耗费的时间)、加上tomcat所消耗的时间，约4s-5s
    
    基于Coody Framework框架的测试用例已在服务器长跑3个月左右未重启：http://118.126.117.84/ (静态资源无法展示由于coody.org外链未备案)

    Coody Framework框架包含以下模块

        coody-core ：框架核心包，包括Ioc+Aop的实现，包括相关工具和超类。
        coody-web  ：框架web-mvc的实现包，实现了mvc功能体系。
        coody-cache：框架缓存的实现，实现了基础缓存，切面缓存，并提供相关切面技术的支持。
        coody-jdbc ：框架orm的实现，实现了基于mysql下基础操作的封装，围绕OOP思想实现了各种简易操作，以高可用后端为原则，拓展了切面事物
        coody-task ：框架定时任务的实现，基于cron实现了定时任务，提供了注解定时任务的支持。
        coody-rcc  ：框架分布式的实现，常规分布式远程调用，提供了注册中心、序列化、通信协议等接口。完成了字节码创建子类的实现(分布式功能未完成)

#### 更新记录：

    2018-02-24： 初步研发，实现Ioc+Aop。

    2018-02-25： 拓展Mvc，基于MVC结合Ioc+Aop实现轻量化(类spring)开发模式

    2018-02-26： 由最初的常规java project整改为Maven Project，并整合自写ORM框架配合aop实现事物管理。

    2018-03-08： 拓展接口形式依赖注入，对interface的Ioc提供支持。

    2018-03-10： 拓展定时任务，基于切面通过注解(cron)实现任务调度。

    2018-03-21： 使用Coody Framework整合至一个外置项目中并成功运行。

    2018-03-21： 为切面的拦截条件新增类通配、方法通配，同时支持一个拦截方法通过多个注解引入不同的拦截规则。

    2018-04-22： 微调Mvc相关功能，支持request和response在controller中进行注入(线程安全)

    2018-04-22： 整改Mvc参数适配器，支持自定义参数适配。

    2018-06-02： 筹划拓展分布式体系，并引入字节码技术创建实现类，提供通讯、序列化、注册中心等接口

    2018-06-05： 项目拆分成Maven多模块化模式，为发布maven中央仓库做准备

    2018-06-28： 发布Alpha至Maven中央仓库。nexus搜索"Coody"即可

    Coody Framework实战项目：https://gitee.com/coodyer/czone/

引用地址：

```
        <dependency>
			<groupId>org.coody.framework</groupId>
			<artifactId>coody-core</artifactId>
			<version>alpha-1.0.4</version>
		</dependency>

		<dependency>
			<groupId>org.coody.framework</groupId>
			<artifactId>coody-jdbc</artifactId>
			<version>alpha-1.0.4</version>
		</dependency>
		<dependency>
			<groupId>org.coody.framework</groupId>
			<artifactId>coody-cache</artifactId>
			<version>alpha-1.0.4</version>
		</dependency>
		<dependency>
			<groupId>org.coody.framework</groupId>
			<artifactId>coody-task</artifactId>
			<version>alpha-1.0.4</version>
		</dependency>
		<dependency>
			<groupId>org.coody.framework</groupId>
			<artifactId>coody-web</artifactId>
			<version>alpha-1.0.4</version>
		</dependency>
```


=======================================================



### 1. 项目背景：


    纵观整个国内开源圈，难以寻找一套比较成熟的Ioc框架。然实现一套Ioc框架并没有太高的技术含量和工作量。

    故此，笔者着手将业余时间的一些代码和案例汇总，研发Coody Framework。



### 2. 功能说明：


    本项目实现注解形式的bean加载、依赖注入、切面等功能。简单实现mvc。
 

### 3. 环境说明：


    JDK1.8+
 

### 4.使用说明：

     **(1)、web.xml说明** 


```

	<!-- 配置扫描的包 -->
	<context-param>
		<param-name>scanPacket</param-name>
		<!-- 逗号分割多个包名 -->
		<param-value>org.coody.czone</param-value>
	</context-param>
	<!-- 配置初始化适配器 -->
	<context-param>
		<param-name>initLoader</param-name>
		<!-- 逗号分割多个加载器 -->
		<param-value>org.coody.framework.web.loader.WebAppLoader,
		org.coody.framework.task.loader.TaskLoader,
		</param-value>
	</context-param>
	<!-- 配置监听器 -->
	<listener>
		<listener-class>org.coody.framework.web.listen.IcopServletListen</listener-class>
	</listener>
	<!-- 初始化分发器 -->
	<servlet>
		<servlet-name>DispatServlet</servlet-name>
		<servlet-class>org.coody.framework.web.DispatServlet</servlet-class>
		<init-param>
			<param-name>viewPath</param-name>
			<param-value>/</param-value>
		</init-param>
	</servlet>
	<!-- MVC配置 -->
	<servlet-mapping>
		<servlet-name>DispatServlet</servlet-name>
		<url-pattern>*.do</url-pattern>
	</servlet-mapping>
```






 **(2)、MVC使用说明** 

    No.1、在scanPacket指定的包目录下某个类使用@PathBinding("/test")注解。传入参数即该类映射路径

    No.2、在方法上面指定@PathBinding("/index.do")注解，传入参数即方法映射路径

    No.3、该方法的访问路径为 /test/index.do

    No.4、映射方法拓展注解@JsonSerialize 被该注解标识的方法将输出json

    No.5、示例：


![输入图片说明](https://gitee.com/uploads/images/2018/0228/085812_e2836fe6_1200611.jpeg "mvc.jpg")


 **(3)、IOC使用说明** 

    No.1、在需要初始化为bean的类上指定@InitBean注解

    No.2、在需要IOC赋值的字段上指定@OutBean

    No.3、示例


![输入图片说明](https://gitee.com/uploads/images/2018/0228/085831_4605c756_1200611.jpeg "ioc1.jpg")


    通过以上注解，完成整个IOC依赖注入的全过程。


 **(4)、切面使用说明** 

     _No.1_ 、自定义一个注解

     _No.2_ 、在需要拦截的方法上面标识该注解

     _No.3_ 、编写一个类，通过@InitBean修饰以实例化bean。在bean内部编写方法。方法用@Around注解修饰。@Around注解传入值为第一步的自定义注解。方法参数类型为AspectWrapper

     _No.4_ 、在如上操作完成后，凡是包含该自定义注解的方法均通过环绕通知

     _No.5_ 、示例：


![输入图片说明](https://gitee.com/uploads/images/2018/0228/085901_39327602_1200611.png "aspect.png")





### 5. 示例：

    
 **依赖注入**  :heart: 

![输入图片说明](https://gitee.com/uploads/images/2018/0314/235137_6318dce2_1200611.png "[}5%ZQRL_T8R62TP5BXDUGF.png")

 **切面编程**   :blue_heart: 

![输入图片说明](https://gitee.com/uploads/images/2018/0314/235227_3d683631_1200611.png "EOC5SKADKLN}P`F`1@]EATS.png")

 **定时任务**  :purple_heart: 

![输入图片说明](https://gitee.com/uploads/images/2018/0314/235300_1427cd5d_1200611.png "ALP63PY8`6MU2B]0AYL}Z16.png")

 **MVC**  :green_heart: 

![输入图片说明](https://gitee.com/uploads/images/2018/0314/235433_0318b322_1200611.png "1GAQ1OY1895$[W2}RF%XCVT.png")

 **事物管理 ** :heartpulse: 

![输入图片说明](https://gitee.com/uploads/images/2018/0314/235504_5666dcbb_1200611.png "$9`DAN2(F25R%_[P@)H$9SS.png")


具体用例详见：https://gitee.com/coodyer/czone

### 6. 版权说明：



    在本项目源代码中，已有测试demo，包括mvc、切面等示例

    作者：Coody
    
    版权：©2014-2020 Test404 All right reserved. 版权所有

    反馈邮箱：644556636@qq.com

    交流群号:218481849

    基于Coody Framework的博文系统：https://gitee.com/coodyer/czone  (研发中)

